#!/usr/bin/make

THIS_FILE := $(lastword $(MAKEFILE_LIST))

.PHONY : up-all

.DEFAULT_GOAL := help

help:
	make -pRrq  -f $(THIS_FILE) : 2>/dev/null | awk -v RS= -F: '/^# File/,/^# Finished Make data base/ {if ($$1 !~ "^[#.]") {print $$1}}' | sort | egrep -v -e '^[^[:alnum:]]' -e '^$@$$'
up:
	docker-compose up -d php redis nginx
	docker-compose exec php bash -c "composer install"
	docker-compose exec php bash -c "php artisan migrate"
	docker-compose run --rm npm sh -c "npm install"
	docker-compose run --rm npm sh -c "npm run prod"
	docker-compose up -d
up-mysql:
	docker-compose up -d mysql
down:
	docker-compose down
stop:
	docker-compose stop php redis mysql nginx queue1 queue2
restart:
	docker-compose restart php redis mysql nginx queue1 queue2