## Survey Drafting

### Routes

### Deployment production

> Read Makefile for details

Raise the mysql base

```
make up-mysql
```

If necessary, fill in a fresh dump of the database

```
docker-compose exec mysql sh
mysql -u root -p --default-character-set=utf8 survey < /mysql/dumpname.sql
```

Raising the whole application

```
make prod
```

How to dump the database

```
docker-compose exec mysql sh
mysqldump -u root -p --default-character-set=utf8 --skip-tz-utc survey > /mysql/dumpname.sql;
```

Add crons to the server

```
* * * * * docker exec php sh -c "php artisan schedule:run"
```

Setting server time

```
sudo timedatectl set-timezone Europe/Chisinau
```
### Local development

#### Installation

```
make dev
```
